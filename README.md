# Gifttt.co
gifttt.co merupakan website yang digunakan oleh customer dan admin. Dengan menggunakan gifttt.co admin dapat menampilkan serta memasarkan produk. Customer dapat melakukan pemesanan dengan memilih prodduk yang telah tersedia di gifttt.co

Anggota yang berkontributor dalam project ini yaitu:
- Hanis Fitriya D S 2000016040
- Dewi Nur A 2000016042
- Retno Ayu Wulandari 2000016063

## Milestones
Berikut Milestone yang telah kami buat sesuai dengan metode pengembangan yang akan kami pakai : 

- [x] [1. Requirements Gathering and Analysis/Planning](https://gitlab.com/hanisftryaa/gifttt.co/-/milestones/2#tab-issues) 
- [x] [2. Quick Design](https://gitlab.com/hanisftryaa/gifttt.co/-/milestones/3#tab-issues)
- [x] [3. Build Prototype](https://gitlab.com/hanisftryaa/gifttt.co/-/milestones/11#tab-issues)
- [ ] [4. Evaluasi Prototype](https://gitlab.com/hanisftryaa/gifttt.co/-/milestones/5#tab-issues)
- [ ] [5. Mengkodekan System](https://gitlab.com/hanisftryaa/gifttt.co/-/milestones/6#tab-issues)
- [ ] [6. Menguji System](https://gitlab.com/hanisftryaa/gifttt.co/-/milestones/7#tab-issues)
- [ ] [7. Evaluasi System](https://gitlab.com/hanisftryaa/gifttt.co/-/milestones/8#tab-issues)
- [ ] [8. Penggunaan System](https://gitlab.com/hanisftryaa/gifttt.co/-/milestones/9#tab-issues)

## Project status
Ongoing
